import { ref, watch } from "vue";
import { defineStore } from "pinia";
import type User from "@/types/User";
import userService from "@/service/user";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";

export const useUserStore = defineStore("user", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const user = ref<User[]>([]);
  const dialog = ref(false);
  const deleteDialog = ref(false);

  const editedUser = ref<User>({ username: "", password: "" });

  watch(dialog, (newDialog) => {
    if (!newDialog) {
      editedUser.value = { username: "", password: "" };
    }
  });

  const getUser = async () => {
    loadingStore.isLoading = true;
    try {
      const res = await userService.getUser();
      user.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล User ได้");
    }
    loadingStore.isLoading = false;
  };

  const saveUser = async () => {
    loadingStore.isLoading = true;
    try {
      if (editedUser.value.id) {
        await userService.updateUser(editedUser.value.id, editedUser.value);
      } else {
        await userService.saveUser(editedUser.value);
      }
      dialog.value = false;
      await getUser();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถบับทึกข้อมูลได้");
    }
    loadingStore.isLoading = false;
  };

  const editUser = (user: User) => {
    editedUser.value = JSON.parse(JSON.stringify(user));
    dialog.value = true;
  };

  const showDeleteDialog = (user: User) => {
    editedUser.value = JSON.parse(JSON.stringify(user));
    deleteDialog.value = true;
  };

  const deleteUser = async () => {
    loadingStore.isLoading = true;
    try {
      await userService.deleteUser(editedUser.value.id!);
      deleteDialog.value = false;
      await getUser();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบข้อมูลได้");
    }
    loadingStore.isLoading = false;
  };

  return {
    user,
    getUser,
    dialog,
    editedUser,
    saveUser,
    editUser,
    deleteUser,
    showDeleteDialog,
    deleteDialog,
  };
});
