import { ref, watch } from "vue";
import { defineStore } from "pinia";
import type Product from "@/types/Product";
import productService from "@/service/product";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";

export const useProductStore = defineStore("product", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const dialog = ref(false);
  const deleteDialog = ref(false);
  const product = ref<Product[]>([]);
  const editedProduct = ref<Product>({ name: "", price: 0 });

  watch(dialog, (newDialog) => {
    if (!newDialog) {
      editedProduct.value = { name: "", price: 0 };
    }
  });

  const getProduct = async () => {
    loadingStore.isLoading = true;
    try {
      const res = await productService.getProduct();
      product.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล Product ได้");
    }
    loadingStore.isLoading = false;
  };
  const saveProduct = async () => {
    loadingStore.isLoading = true;
    try {
      if (editedProduct.value.id) {
        await productService.updateProduct(
          editedProduct.value.id,
          editedProduct.value
        );
      } else {
        await productService.saveProduct(editedProduct.value);
      }
      dialog.value = false;
      await getProduct();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถบับทึกข้อมูลได้");
    }
    loadingStore.isLoading = false;
  };

  const editProduct = (product: Product) => {
    editedProduct.value = JSON.parse(JSON.stringify(product));
    dialog.value = true;
  };

  const showDeleteDialog = (product: Product) => {
    editedProduct.value = JSON.parse(JSON.stringify(product));
    deleteDialog.value = true;
  };

  const deleteProduct = async () => {
    loadingStore.isLoading = true;
    try {
      await productService.deleteProduct(editedProduct.value.id!);
      deleteDialog.value = false;
      await getProduct();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบข้อมูลได้");
    }
    loadingStore.isLoading = false;
  };

  return {
    product,
    getProduct,
    dialog,
    editedProduct,
    saveProduct,
    editProduct,
    deleteProduct,
    deleteDialog,
    showDeleteDialog,
  };
});
