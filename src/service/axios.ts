import axios from "axios";

const instance = axios.create({
  baseURL: "http://localhost:3000",
});

function deley(time: number) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve(true);
    }, time);
  });
}

instance.interceptors.request.use(
  async function (config) {
    await deley(500);
    return config;
  },
  function (error) {
    // Do something with request error
    return Promise.reject(error);
  }
);

export default instance;
