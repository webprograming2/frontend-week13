import http from "@/service/axios";
import type Product from "@/types/Product";
const getProduct = () => {
  return http.get("/products");
};

const saveProduct = (product: Product) => {
  return http.post("/products", product);
};

const updateProduct = (id: number, product: Product) => {
  return http.patch("/products/" + id, product);
};

const deleteProduct = (id: number) => {
  return http.delete("/products/" + id);
};

export default { getProduct, saveProduct, updateProduct, deleteProduct };
