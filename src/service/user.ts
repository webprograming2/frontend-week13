import http from "@/service/axios";
import type User from "@/types/User";
const getUser = () => {
  return http.get("/users");
};

const saveUser = (user: User) => {
  return http.post("/users", user);
};

const updateUser = (id: number, user: User) => {
  return http.patch("/users/" + id, user);
};

const deleteUser = (id: number) => {
  return http.delete("/users/" + id);
};

export default { getUser, saveUser, updateUser, deleteUser };
